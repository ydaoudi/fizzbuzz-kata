import java.util.HashMap;
import java.util.Map;

public class FizzBuzz {

    public Map<Integer, String> substitutionMap = new HashMap<Integer, String>();

    public FizzBuzz(){
        //Initial DATA
        substitutionMap.put(3, "Fizz");
        substitutionMap.put(5, "Buzz");
        substitutionMap.put(7, "Pop");
    }

    public String executeGame(Integer number) {
        String res = "";
        for(Map.Entry mapEntry : this.substitutionMap.entrySet()){
            if(number % (Integer)mapEntry.getKey() == 0){
                res =  res+mapEntry.getValue().toString();
            }
        }
        if("".equals(res)){
            return String.valueOf(number);
        }
        else return res;
    }

    public Map<Integer, String> customSubstitution(Integer valueToSubstitute, String substitution){
        this.substitutionMap.put(valueToSubstitute, substitution);
        return this.substitutionMap;
    }
}
