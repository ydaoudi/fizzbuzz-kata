import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class FizzBuzzTest {

    private FizzBuzz fb;

    @BeforeEach
    public void runGame(){
        fb = new FizzBuzz();
    }

    @DisplayName("Play FizzBuzz with 1")
    @Test
    public void return1IfNumberIs1(){
        String fizzBuzz = fb.executeGame(1);
        Assertions.assertEquals(fizzBuzz, "1");
    }

    @DisplayName("Play FizzBuzz with 2")
    @Test
    public void return2IfNumberIs2(){
        String fizzBuzz = fb.executeGame(2);
        Assertions.assertEquals(fizzBuzz, "2");
    }

    @DisplayName("Play FizzBuzz : Multiple of three return Fizz")
    @Test
    public void multipleOfThreeReturnFizz(){
        String fizzBuzzWith3 = fb.executeGame(3);
        String fizzBuzzWith9 = fb.executeGame(9);
        String fizzBuzzWith123 = fb.executeGame(123);
        Assertions.assertEquals(fizzBuzzWith3, "Fizz");
        Assertions.assertEquals(fizzBuzzWith9, "Fizz");
        Assertions.assertEquals(fizzBuzzWith123, "Fizz");
    }

    @DisplayName("Play FizzBuzz : Multiple of five return Buzz")
    @Test
    public void multipleOfFiveReturnBuzz(){
        String fizzBuzzWith5 = fb.executeGame(5);
        String fizzBuzzWith20 = fb.executeGame(20);
        String fizzBuzzWith200 = fb.executeGame(200);
        Assertions.assertEquals(fizzBuzzWith5, "Buzz");
        Assertions.assertEquals(fizzBuzzWith20, "Buzz");
        Assertions.assertEquals(fizzBuzzWith200, "Buzz");
    }

    @DisplayName("Play FizzBuzz : Multiple of three and five return FizzBuzz")
    @Test
    public void multipleOfThreeAndFiveReturnFizzBuzz(){
        String fizzBuzzWith15 = fb.executeGame(15);
        String fizzBuzzWith45 = fb.executeGame(45);
        String fizzBuzzWith315 = fb.executeGame(315);
        Assertions.assertEquals(fizzBuzzWith15, "FizzBuzz");
        Assertions.assertEquals(fizzBuzzWith45, "FizzBuzz");
    }

    @DisplayName("Play FizzBuzz : Multiple of seven return Pop")
    @Test
    public void multipleOfSevenReturnPop(){
        String fizzBuzzWith7 = fb.executeGame(7);
        String fizzBuzzWith28 = fb.executeGame(28);
        String fizzBuzzWith77 = fb.executeGame(77);
        Assertions.assertEquals(fizzBuzzWith7, "Pop");
        Assertions.assertEquals(fizzBuzzWith28, "Pop");
        Assertions.assertEquals(fizzBuzzWith77, "Pop");
    }

    @DisplayName("Play FizzBuzz : Multiples of Three and Seven Return FizzPop")
    @Test
    public void multipleOfSevenAndThreeReturnFizzPop(){
        String fizzBuzzWith21 = fb.executeGame(21);
        String fizzBuzzWith63 = fb.executeGame(63);
        String fizzBuzzWith126 = fb.executeGame(126);
        Assertions.assertEquals(fizzBuzzWith21, "FizzPop");
        Assertions.assertEquals(fizzBuzzWith63, "FizzPop");
        Assertions.assertEquals(fizzBuzzWith126, "FizzPop");
    }

    @DisplayName("Play FizzBuzz : Multiples of Five and Seven Return BuzzPop")
    @Test
    public void multipleOfSevenAndFiveReturnBuzzPop(){
        String fizzBuzzWith35 = fb.executeGame(35);
        String fizzBuzzWith70 = fb.executeGame(70);
        String fizzBuzzWith140 = fb.executeGame(140);
        Assertions.assertEquals(fizzBuzzWith35, "BuzzPop");
        Assertions.assertEquals(fizzBuzzWith70, "BuzzPop");
        Assertions.assertEquals(fizzBuzzWith140, "BuzzPop");
    }

    @DisplayName("Play FizzBuzz : Multiples of Three, Five and Seven Return FizzBuzzPop")
    @Test
    public void multipleOfSevenAndFiveAndThreeReturnFizzBuzzPop(){
        String fizzBuzzWith105 = fb.executeGame(105);
        String fizzBuzzWith210 = fb.executeGame(210);
        String fizzBuzzWith315 = fb.executeGame(315);
        Assertions.assertEquals(fizzBuzzWith105, "FizzBuzzPop");
        Assertions.assertEquals(fizzBuzzWith210, "FizzBuzzPop");
        Assertions.assertEquals(fizzBuzzWith315, "FizzBuzzPop");
    }

    @DisplayName("Play FizzBuzz : Using a Custom Substitution")
    @Test
    public void customSubstitution(){
        fb.customSubstitution(2,"Fuzz");
        String fizzBuzzWith1 = fb.executeGame(1);
        String fizzBuzzWith2 = fb.executeGame(2);
        String fizzBuzzWith8 = fb.executeGame(8);
        Assertions.assertEquals(fizzBuzzWith1, "1");
        Assertions.assertEquals(fizzBuzzWith2, "Fuzz");
        Assertions.assertEquals(fizzBuzzWith8, "Fuzz");
    }

    @DisplayName("Play FizzBuzz : Using a Custom Substitution together")
    @Test
    public void customSubstitutionTogether(){
        fb.customSubstitution(2,"Fuzz");
        fb.customSubstitution(3,"Bizz");
        String fizzBuzzWith4 = fb.executeGame(4);
        String fizzBuzzWith9 = fb.executeGame(9);
        String fizzBuzzWith12 = fb.executeGame(12);
        Assertions.assertEquals(fizzBuzzWith4, "Fuzz");
        Assertions.assertEquals(fizzBuzzWith9, "Bizz");
        Assertions.assertEquals(fizzBuzzWith12, "FuzzBizz");
    }




}
